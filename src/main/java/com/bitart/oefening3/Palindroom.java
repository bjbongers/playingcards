package com.bitart.oefening3;


public class Palindroom {


    public boolean evaluate(String s) {

        if (s.length() < 2) {
            return true;
        }

        String first = firstLetter(s);
        String last = lastLetter(s);
        String chopped = chopFirstAndLast(s);

        // iets is een palindroom:
        // 1: als de eerste letter en de laatste letter gelijk zijn
        // 2: als de binnenste ook een palindroom is.

        if (first.equals(last) && evaluate(chopped)) {
            return true;
        } else {
            return false;
        }
        //return (first.equals(last)) && palindrome(chopped);
    }

    private String chopFirstAndLast(String s) {
        if (s.length() < 2) {
            return s;
        }
        return s.substring(1, s.length() - 1);
    }

    private String firstLetter(String s) {
        return s.substring(0, 1);
    }

    // s  = arthur
    // lengte = 6
    // s.substring(5,6) ==> r
    private String lastLetter(String s) {
        int lengte = s.length();
        return s.substring(lengte - 1, lengte);
    }
}
