package com.bitart.oefening3;


public class MaxOdd {


    public int bepaalMaxOdd(int[] r, int n) {
        if (n >= r.length) {
            return 0;
        }

        int currentElement = r[n];

        if (isOdd(currentElement)) {

            // currentElement is oneven
            int maximaleOnevenVanDeRest = bepaalMaxOdd(r, n + 1);
            if (currentElement > maximaleOnevenVanDeRest) {
                return currentElement;
            } else {
                return maximaleOnevenVanDeRest;
            }
            //return Math.max(currentElement, maxOdd(r, n + 1));
        } else {
            // currentElement is even
            return bepaalMaxOdd(r, n + 1);
        }
    }

    private boolean isOdd(int input) {
        return (input % 2) == 1;
    }
}
