package com.bitart.oefening2;


import java.util.ArrayList;
import java.util.List;


public class BlackJack {

    private static final int BLACKJACK_VALUE = 21;

    /**
     * Write a method boolean drawBlackjack(int n),
     * 1: that creates a new deck
     * 2: shuffles it,
     * 3: draws n cards,
     * 4: and returns whether or not those n cards form a blackjack.
     * This method will be in a program class, and not in the class PlayingCard or Deck.
     * You don’t have to specify this.
     */
    public boolean drawBlackjack(int n) {

        Deck mydeck = new Deck();
        mydeck.shuffle();

        List<PlayingCard> hand = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            PlayingCard oneCard = mydeck.draw();
            hand.add(oneCard);
        }

        return isBlackJack(hand);
    }

    private boolean isBlackJack(List<PlayingCard> hand) {

        int sum = 0;
        String logger = "";
        // ga de waardes van alle kaarten optellen.
        for (int i = 0; i < hand.size(); i++) {
            PlayingCard oneCard = hand.get(i);
            int pointValue = oneCard.getPointValue();
            sum += pointValue;
            logger = logger + oneCard.toString() + "\n";
        }


        System.out.println("De som van : \n" + logger + " = " + sum);
        return (sum == BLACKJACK_VALUE);
    }
}
