package com.bitart.oefening2;


import java.util.ArrayList;
import java.util.List;

import static com.bitart.oefening2.PlayingCard.SUITS;
import static com.bitart.oefening2.PlayingCard.RANKS;


public class Deck {

    static final int SHUFFLE_SIZE = 150;

    private final List<PlayingCard> cardsAsList;

    public Deck() {
        cardsAsList = new ArrayList<>();

        for (int p = 0; p <= SUITS.length - 1; p++) {
            for (int k = 0; k <= RANKS.length - 1; k++) {
                PlayingCard card = new PlayingCard(SUITS[p], RANKS[k]);
                cardsAsList.add(card);
            }
        }
    }

    public PlayingCard draw() {
        PlayingCard last = cardsAsList.get(cardsAsList.size() - 1);
        cardsAsList.remove(last);
        return last;
    }


    public void shuffle() {
        for (int k = 1; k <= SHUFFLE_SIZE; k++) {

            int index1 = random(cardsAsList.size() - 1);
            int index2 = random(cardsAsList.size() - 1);

            swapCards(index1, index2);
        }
    }

    private void swapCards(int index1, int index2) {

        if (index1 == index2) {
            return;
        }

        PlayingCard card1 = cardsAsList.get(index1);
        PlayingCard card2 = cardsAsList.get(index2);

        cardsAsList.remove(index2);
        cardsAsList.add(index2, card1);

        cardsAsList.remove(index1);
        cardsAsList.add(index1, card2);
    }

    private int random(int max) {
        return (int) (Math.random() * max);
    }

    @Override
    public String toString() {
        String s = "";

        for (PlayingCard card : cardsAsList) {
            s = s + card.toString() + "\n";
        }

        return s;
    }
}
