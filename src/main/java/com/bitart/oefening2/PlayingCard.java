package com.bitart.oefening2;


public class PlayingCard {

    static final String[] SUITS = {"clubs", "diamonds", "hearts", "spades"};
    static final int[]    RANKS = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};

    private final String suit;
    private final int    rank;

    PlayingCard(String suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public int getRank() {
        return rank;
    }

    public int getPointValue() {
        if (getRank() == 11 || getRank() == 12 || getRank() == 13) {
            return 10;
        }
        if (getRank() == 14) {
            return 11;
        }
        return getRank();
    }

    @Override
    public String toString() {
        return "PlayingCard{" +
                "suit='" + suit + '\'' +
                ", rank=" + rank +
                '}';
    }
}

