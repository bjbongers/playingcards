package com.bitart.oefening1;

import java.io.PrintStream;


class DetermineOutput {

    PrintStream out = new PrintStream(System.out);

    int a = 4;
    int b = 2;

    public void start() {

        println(a, b);

        a = method1(a);
        println(a, b);

        b = method2(b, a);
        println(a, b);
    }

    void println(int a, int b) {
        out.printf("%d - %d\n", a, b);
    }

    private int method1(int a) {
        a = a / b;
        b = b + a;
        println(a, b);
        return a;
    }

    private int method2(int b, int c) {
        int a = b;
        b = b - c;
        c = method1(b + c);
        println(c, a);
        return b;
    }
}
