package com.bitart.oefening3;


import org.junit.jupiter.api.Test;


public class PalindroomTest {


    @Test
    void evaluate() {

        Palindroom palindroom = new Palindroom();

        System.out.println("palindrome(noon) = " + palindroom.evaluate("noon"));
        System.out.println("palindrome(aracecarb) = " + palindroom.evaluate("aracecarb"));
        System.out.println("palindrome(A) = " + palindroom.evaluate("A"));
        System.out.println("palindrome() = " + palindroom.evaluate(""));
        System.out.println("palindrome(barttrab) = " + palindroom.evaluate("barttrab"));
        System.out.println("palindrome(pp) = " + palindroom.evaluate("pp"));
        System.out.println("palindrome(pap) = " + palindroom.evaluate("pap"));
        System.out.println("palindrome(paps) = " + palindroom.evaluate("paps"));
    }
}
