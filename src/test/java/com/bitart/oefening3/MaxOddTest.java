package com.bitart.oefening3;


import org.junit.jupiter.api.Test;


public class MaxOddTest {


    @Test
    void bepaalMaxOdd() {

        MaxOdd maxOdd = new MaxOdd();
        System.out.println("maxodd{6, 4, 5, 3}, 0) = " + maxOdd.bepaalMaxOdd(new int[] {6, 4, 5, 3}, 0));
        System.out.println("maxodd{5, 4, 3, 2}, 2) = " + maxOdd.bepaalMaxOdd(new int[] {5, 4, 3, 2}, 2));
        System.out.println("maxodd{2, 4, 6, 8}, 0) = " + maxOdd.bepaalMaxOdd(new int[] {2, 4, 6, 8}, 0));
    }
}
