package com.bitart.oefening2;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class DrawCardsTest {

    @Test
    void createDeck() {
        Deck deck = new Deck();
        assertThat(deck).isNotNull();
        System.out.println(deck.toString());
    }

    @Test
    void draw() {
        Deck deck = new Deck();
        PlayingCard oneCard = deck.draw();
        assertThat(oneCard).isNotNull();
        System.out.println(oneCard.toString());
    }

    @Test
    void shuffle() {
        Deck deck = new Deck();
        deck.shuffle();
        System.out.println(deck.toString());
    }
}
